 /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */


function getProjectByStatus(data) {
  let getAllProjects = data.reduce((accumulator, person) => {
      if (person.projects) {
          accumulator.push(person.projects);
      }
      return accumulator;
  }, []).flat();

  let projectsByStatus = getAllProjects.reduce((accumulator, project) => {
      if (!accumulator[project.status]) {
          accumulator[project.status] = [project.name];
      } else {
          accumulator[project.status].push(project.name);
      }
      return accumulator;
  }, {});

  return projectsByStatus;
}

module.exports=getProjectByStatus