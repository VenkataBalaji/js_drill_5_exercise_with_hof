// Iterate over each object in the array and in the email breakdown the email and return the output as below:
  /*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */
     
     function getEmailDetails(email, data) {
      if(Array.isArray(data)){
       let userDetails = data.map((element) => {
         let temp = {};
         let splitNames = element.name.split(" ");
         temp.firstName = splitNames[0];
         temp.lastName = splitNames[1];
         temp.email = element.email;
         return temp;
       });
       return userDetails;
      }else{
         console.log("In Suffient Data")
      }
     }
     
    

     module.exports=getEmailDetails;


